#!/usr/bin/python
# -*- encoding: utf-8 -*-

import codecs   # Lib responsavel por gerar o arquivo
import urllib2  # Lib responsavel por acessar a pagina
from bs4 import BeautifulSoup  # Lib responsavel transformar os dados da pagina em objetos

# Url acessada
url = 'http://g1.globo.com/economia/mercados/cotacoes/moedas/'
request = urllib2.urlopen(url)

# Transformando a pagina em objeto
soup = BeautifulSoup(request, 'html.parser', from_encoding='utf-8')

# Extraindo dados da tabela da página
trs = soup.find_all('tr')
conteudo = ''
for tr in trs:
    for td in tr:
        conteudo += td.text + ';'
    conteudo += '\n'

# Escrevendo o conteúdo extraido no arquivo csv
arquivo = codecs.open('/tmp/cotacao.csv', 'w', 'utf-8')
arquivo.write(conteudo)


